cmake_minimum_required(VERSION 2.8)
project(luasol)
SET(CMAKE_BUILD_TYPE Debug)
#set(CMAKE_CXX_STANDARD 14)

#SET(CMAKE_CXX_FLAGS "-fPIC -shared -Wno-deprecated")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}  -std=c++14")

INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR})
#INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/lua-5.1.5)
INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/lua-5.1.5/src)
INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/sol2-2.18.5/single/sol)

AUX_SOURCE_DIRECTORY(${CMAKE_SOURCE_DIR}/lua-5.1.5/src dir_lua)
AUX_SOURCE_DIRECTORY(. dir_main)

add_executable(luasol ${dir_main} ${dir_lua} )