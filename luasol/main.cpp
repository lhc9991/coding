#include <iostream>
#define SOL_USING_CXX_LUA
#include "sol.hpp"
#include <cassert>
#include <memory.h>

using namespace std;


void call_stack(lua_State* L, int n) {
    printf("call_stack start \n");
    if (NULL == L) {
        printf("call_stack no L\n");
        return;
    }
    lua_Debug ar;
    if (lua_getstack(L, n, &ar) == 1) {
        lua_getinfo(L, "nSlu", &ar);

        if (n == 0) {
            printf("<call lua stack>\n");
        } else {
        }

        if (ar.name)
            printf("lua_stack -- %s() : line %d [%s : line %d]\n", ar.name, ar.currentline, ar.source, ar.linedefined);
        else
            printf("lua_stack -- unknown : line %d [%s : line %d]\n", ar.currentline, ar.source, ar.linedefined);

        call_stack(L, n + 1);
    }
}


class A
{
public:
    A(){
        for (int i = 0; i < 5; ++i) {
            vec_list1.push_back(i+1);
        }

        std::cout <<  "constructor A" << std::endl;
    };
    ~A(){
        std::cout <<  "destruct  A" << std::endl;
    }

public:
    bool funcA()
    {
        std::cout << "funcA........" << std::endl;
    }

    string funcStr()
    {
        std::cout << "funcA........" << std::endl;
        return stra1;
    }

    virtual bool funcBase()
    {
        std::cout << "funcBase...1....." << std::endl;
    }

public:
    vector<int> vec_list1;
    string stra1;
};


class B : public A
{
public:
    B(){};
    ~B(){};

public:
    bool funcB()
    {
        std::cout << "funcB........" << std::endl;
    }

    bool funcBase()
    {
        std::cout << "funcBase...2....." << std::endl;
    }
};

class classC
{
public:
    classC(){};
    ~classC(){};
    static classC& instance()
    {
        static classC ins;
        return ins;
    }

public:
    bool funcC()
    {
        std::cout << "funcC........" << std::endl;
        return false;
    }
};

void global_func1()
{
    std::cout << "global_func1........" << std::endl;
}

string global_func2()
{
    std::cout << "global_func2........" << std::endl;
    string str1 = "fwfwfwf";
    return str1;
}

int main()
{
    sol::state sol_lua;
    sol_lua.open_libraries(sol::lib::base);



    //sol_lua["funcC"] = classC::instance().funcC;
    sol_lua.set_function("global_func1", global_func1);
    sol_lua.set_function("global_func2", global_func2);

//    sol_lua.new_usertype<A>("A",
//                            "funcA", &A::funcA
//    );
    //sol_lua.set("A",A{});
    sol_lua.set_function("funcA", &A::funcA,A{});

    sol_lua.new_usertype<classC>("classC",
                              "new", sol::no_constructor,
                              "instance", &classC::instance,
                              "funcC", &classC::funcC
    );

    A classA;
    sol_lua["lua_vec"] = classA.vec_list1;


    try
    {
        //sol::load_result rst = sol_lua.load_file("../test1.lua");
        sol::protected_function_result rst = sol_lua.do_file("../test1.lua");
        if(rst.valid() == false)
        {
            printf("lua load valid:%d index:%d file-> %s error-> %s \n",rst.valid(), rst.stack_index(),"filename", lua_tostring(rst.lua_state(), rst.stack_index()));
            return 0;
        }
    }
    catch(sol::error& e)
    {
        std::cout << "catch........" << std::endl;
        return 0;
    }

    //return 0;

    /********************call lua func ******************************/

//    try
//    {
//        sol::function func_rst = sol_lua["c_call_funB"];
//        func_rst();
//    }
//    catch (sol::error& e)
//    {
//        std::cout << "catch...sol::error....." <<  e.what() << std::endl;
//        //printf("catch error-> %s \n",lua_tostring(sol_lua.lua_state(), -1));
//        call_stack(sol_lua.lua_state(),0);
//    }

    try{
        sol::protected_function f = sol_lua["c_call_funB"];
        //sol::protected_function_result result = f(1,"a1",false);
        auto result = f(1,"a1",false);
        if (result.valid()) {
            // Call succeeded
            //int x = result;

            int r1;
            int r2;
            int r3;
            //std::tuple<int, int, int> sss  = result;
            int aaa = result.pop_count();
            int return_count = result.return_count();
            int aaaa = result.get<int>();
            std::tuple<int, int, int> sss = result.get<std::tuple<int, int, int>>();
            std::cout << "protected_function succeeded, result is " << sol::to_string(result.status()) << std::endl;
        }
        else {
            // Call failed
            sol::error err = result;
            std::string what = err.what();
            std::cout << "protected_function failed, sol::error::what() is " << what << std::endl;
            //call_stack(sol_lua.lua_state(),0);
            // 'what' Should read
            // "Handled this message: negative number detected"
        }
    }
    catch (sol::error& e)
    {
        std::cout << "protected_function catch......." <<  e.what() << std::endl;
        //printf("catch error-> %s \n",lua_tostring(sol_lua.lua_state(), -1));
    }


//    try
//    {
//        sol::table luaTb = sol_lua["test1"];
//        luaTb["c_call_fun"]();
//    }
//    catch (sol::error& e)
//    {
//        std::cout << "catch2...sol::error....." <<  e.what() << std::endl;
//        //printf("catch error2-> %s \n",lua_tostring(sol_lua.lua_state(), -1));
//    }

    //std::tuple<int,int,int> trst;
    std::cout << "Hello, World!" << std::endl;

    return 0;
}