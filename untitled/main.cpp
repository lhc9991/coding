#include <iostream>

class base
{
public:
    base() {

    };
    ~base(){

    };

    virtual bool init(){return false;};
};

class A : public base
{
public:
    A() {

    };
    ~A(){

    }

    int v1;
    int v2;
};

class B : public base
{
public:
    B() {

    };
    ~B() {

    };

    int x1;
    std::string x2;
};


class C {
public:
    C() {

    };
    ~C(){

    }

    std::string v2;
    short v1;
};

class D {
public:
    D() {

    };
    ~D(){

    }

    std::string v2;
    int v1;
};

int main()
{

    A* a1 = new A();
    a1->v1 = 2222;
    a1->v2 = 3333;

    base* base1 = (base*)a1;
//
    int* fsfsf = reinterpret_cast<int*>(base1);

    B* b11 = static_cast<B*>(base1);
    B* b33 = dynamic_cast<B*>(base1);

    //B* b22 = reinterpret_cast<B*>(base1);

    A* a11 = static_cast<A*>(base1);
    A* a22 = dynamic_cast<A*>(base1);


    //B* b111 = static_cast<B*>(a1);
    B* b333 = dynamic_cast<B*>(a1);
    B* b222 = reinterpret_cast<B*>(a1);

//    C* c11 = static_cast<C*>(a1);
//    C* c22 = reinterpret_cast<C*>(a1);
//    C* c33 = dynamic_cast<C*>(a1);


    D* d1 = new D();
    d1->v1 = 11;
//    C* c2 = static_cast<C*>(d1);
//    C* c3 = dynamic_cast<C*>(d1);
//    C* c4 = reinterpret_cast<C*>(d1);

    B* b1 = (B*)a1;


    std::cout << "Hello, World!" << std::endl;
    return 0;
}